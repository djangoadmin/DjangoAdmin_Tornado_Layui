# +----------------------------------------------------------------------
# | DjangoAdmin敏捷开发框架 [ 赋能开发者，助力企业发展 ]
# +----------------------------------------------------------------------
# | 版权所有 2021~2025 北京DjangoAdmin研发中心
# +----------------------------------------------------------------------
# | Licensed Apache-2.0 DjangoAdmin并不是自由软件，未经许可禁止去掉相关版权
# +----------------------------------------------------------------------
# | 官方网站: https://www.djangoadmin.cn
# +----------------------------------------------------------------------
# | 作者: @一米阳光 团队荣誉出品
# +----------------------------------------------------------------------
# | 版权和免责声明:
# | 本团队对该软件框架产品拥有知识产权（包括但不限于商标权、专利权、著作权、商业秘密等）
# | 均受到相关法律法规的保护，任何个人、组织和单位不得在未经本团队书面授权的情况下对所授权
# | 软件框架产品本身申请相关的知识产权，禁止用于任何违法、侵害他人合法权益等恶意的行为，禁
# | 止用于任何违反我国法律法规的一切项目研发，任何个人、组织和单位用于项目研发而产生的任何
# | 意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、
# | 附带或衍生的损失等)，本团队不承担任何法律责任，本软件框架禁止任何单位和个人、组织用于
# | 任何违法、侵害他人合法利益等恶意的行为，如有发现违规、违法的犯罪行为，本团队将无条件配
# | 合公安机关调查取证同时保留一切以法律手段起诉的权利，本软件框架只能用于公司和个人内部的
# | 法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
# +----------------------------------------------------------------------

import tornado

# from apps.widgets import init_widgets
from tornado.options import define

from extends import register_extends
from config.env import TORNADO_PORT

from routers.router import init_routers

define('port', default=TORNADO_PORT, help='run on the given port', type=int)


# 创建应用实例
def create_app():
    # 应用配置
    app = tornado.web.Application(
        # 初始化路由
        init_routers(),
        # 模板文件目录
        template_path='templates',
        # 静态文件的路径
        static_path='static',
        # 静态文件的路由
        static_url_prefix='/static/',
        # 开启调试模式
        debug=True,
        # 设置True后自动重启,不必每次重启服务
        autoreload=False,
        # 支持gzip压缩
        gzip=True,
        # 禁用自动转义
        autoescape=None,
        # 设置防跨站请求攻击
        xsrf_cookies=None,
        # COOKIE秘钥
        cookie_secret="61oETzKXQAGaYdkL5gEmGeJJFuYh7EQnp2XdTP1o/Vo=",
        # 模板函数
        # ui_methods=init_widgets()
    )

    # 初始化扩展
    register_extends(app)

    # 允许在命令行启动
    tornado.options.parse_command_line()
    # 创建应用服务
    http_server = tornado.httpserver.HTTPServer(app)
    # 设置服务端的监听端口
    http_server.listen(tornado.options.options.port)
    # 启动应用
    tornado.ioloop.IOLoop.current().start()
    # 返回应用
    return app
