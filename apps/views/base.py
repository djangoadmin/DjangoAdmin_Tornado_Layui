# +----------------------------------------------------------------------
# | DjangoAdmin敏捷开发框架 [ 赋能开发者，助力企业发展 ]
# +----------------------------------------------------------------------
# | 版权所有 2021~2025 北京DjangoAdmin研发中心
# +----------------------------------------------------------------------
# | Licensed Apache-2.0 DjangoAdmin并不是自由软件，未经许可禁止去掉相关版权
# +----------------------------------------------------------------------
# | 官方网站: https://www.djangoadmin.cn
# +----------------------------------------------------------------------
# | 作者: @一米阳光 团队荣誉出品
# +----------------------------------------------------------------------
# | 版权和免责声明:
# | 本团队对该软件框架产品拥有知识产权（包括但不限于商标权、专利权、著作权、商业秘密等）
# | 均受到相关法律法规的保护，任何个人、组织和单位不得在未经本团队书面授权的情况下对所授权
# | 软件框架产品本身申请相关的知识产权，禁止用于任何违法、侵害他人合法权益等恶意的行为，禁
# | 止用于任何违反我国法律法规的一切项目研发，任何个人、组织和单位用于项目研发而产生的任何
# | 意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、
# | 附带或衍生的损失等)，本团队不承担任何法律责任，本软件框架禁止任何单位和个人、组织用于
# | 任何违法、侵害他人合法利益等恶意的行为，如有发现违规、违法的犯罪行为，本团队将无条件配
# | 合公安机关调查取证同时保留一切以法律手段起诉的权利，本软件框架只能用于公司和个人内部的
# | 法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
# +----------------------------------------------------------------------

import json

import tornado

from jinja2 import Environment, FileSystemLoader

from apps.middleware.login_required import login_required
from apps.widgets import register_widget

from extends.extends_session import Session


# 基类执行句柄
class BaseHandler(tornado.web.RequestHandler):

    # 初始化
    def initialize(self):
        self.session = Session(self)

    def __init__(self, application, request, **kwargs):
        super(BaseHandler, self).__init__(application, request, **kwargs)

    def data_received(self, chunk):
        pass

    def try_get_json(self):
        try:
            body_str = self.request.body.decode('utf8')
            data = json.loads(body_str)
            return data
        except ValueError:
            return None

    # 预请求时统一进行登录认证
    @login_required
    def prepare(self):
        pass

    # # @db_session
    # def get_current_user(self):
    #     """
    #     check token is valid or not, if valid, return user
    #     """
    #     now = datetime.now()
    #     token = self.request.headers.get("Auth-Token", "")
    #     if not token:
    #         return None
    #     user = select(a.user for a in AuthToken if a.token == token and a.expire_time > now).first()
    #     return user

    # 重写模板渲染方法
    def render_template(self, template_name, **kwargs):
        template_dirs = []
        template_dirs.append(self.settings['template_path'])
        env = Environment(loader=FileSystemLoader(template_dirs))
        # 这里可以自定义过滤器
        # env.filters['func_name'] = func_name
        # 注册核心组件
        register_widget(env)
        kwargs.update({
            'settings': self.settings,
            'static_url': self.static_url,
            'request': self.request,
            'current_user': self.current_user,
            'xsrf_token': self.xsrf_token,
            'xsrf_form_html': self.xsrf_form_html,
        })
        template = env.get_template(template_name)
        html = template.render(kwargs)
        self.write(html)
        # return html
