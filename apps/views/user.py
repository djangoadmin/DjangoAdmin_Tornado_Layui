# +----------------------------------------------------------------------
# | DjangoAdmin敏捷开发框架 [ 赋能开发者，助力企业发展 ]
# +----------------------------------------------------------------------
# | 版权所有 2021~2025 北京DjangoAdmin研发中心
# +----------------------------------------------------------------------
# | Licensed Apache-2.0 DjangoAdmin并不是自由软件，未经许可禁止去掉相关版权
# +----------------------------------------------------------------------
# | 官方网站: https://www.djangoadmin.cn
# +----------------------------------------------------------------------
# | 作者: @一米阳光 团队荣誉出品
# +----------------------------------------------------------------------
# | 版权和免责声明:
# | 本团队对该软件框架产品拥有知识产权（包括但不限于商标权、专利权、著作权、商业秘密等）
# | 均受到相关法律法规的保护，任何个人、组织和单位不得在未经本团队书面授权的情况下对所授权
# | 软件框架产品本身申请相关的知识产权，禁止用于任何违法、侵害他人合法权益等恶意的行为，禁
# | 止用于任何违反我国法律法规的一切项目研发，任何个人、组织和单位用于项目研发而产生的任何
# | 意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、
# | 附带或衍生的损失等)，本团队不承担任何法律责任，本软件框架禁止任何单位和个人、组织用于
# | 任何违法、侵害他人合法利益等恶意的行为，如有发现违规、违法的犯罪行为，本团队将无条件配
# | 合公安机关调查取证同时保留一切以法律手段起诉的权利，本软件框架只能用于公司和个人内部的
# | 法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
# +----------------------------------------------------------------------

from apps.middleware.permission_required import permission_required
from apps.models.dept import Dept
from apps.models.level import Level
from apps.models.position import Position
from apps.models.role import Role
from apps.services import user
from apps.views.base import BaseHandler
from config.env import TORNADO_DEMO
from extends import db
from utils import R


# 渲染主页
class UserIndexHandler(BaseHandler):
    @permission_required("sys:user:list")
    async def get(self):
        # 渲染模板
        return self.render_template('user/index.html')


# 查询分页数据
class UserListHandler(BaseHandler):
    @permission_required("sys:user:list")
    async def get(self):
        # 调用查询分页数据服务方法
        result = await user.UserList(self)
        # 返回结果
        return result


# 查询用户详情
class UserDetailHandler(BaseHandler):
    @permission_required("sys:user:detail")
    async def get(self, id):
        try:
            # 调用查询用户详情服务方法
            data = await user.UserDetail(id)
            # 查询角色列表
            role_list = db.query(Role).filter(Role.is_delete == 0).all()
            roleList = []
            for item in role_list:
                roleList.append(item.to_dict())
            # 查询部门列表
            dept_list = db.query(Dept).filter(Dept.is_delete == 0).all()
            deptList = []
            for item in dept_list:
                deptList.append(item.to_dict())
            # 查询职级列表
            level_list = db.query(Level).filter(Level.is_delete == 0).all()
            levelList = []
            for item in level_list:
                levelList.append(item.to_dict())
            # 查询用户列表
            position_List = db.query(Position).filter(Position.is_delete == 0).all()
            positionList = []
            for item in position_List:
                positionList.append(item.to_dict())
            # 渲染模板并绑定参数
            return self.render_template('user/edit.html', data=data, roleList=roleList, deptList=deptList,
                                        levelList=levelList, positionList=positionList)
        except:
            # 抛出异常
            raise
        finally:
            # 关闭连接
            db.close()


# 添加用户
class UserAddHandler(BaseHandler):
    @permission_required("sys:user:add")
    async def post(self):
        if TORNADO_DEMO:
            return R.failed(self, "演示环境，暂无操作权限")
        # 调用添加用户服务
        result = await user.UserAdd(self)
        # 返回结果
        return result


# 更新用户
class UserUpdateHandler(BaseHandler):
    @permission_required("sys:user:update")
    async def put(self):
        if TORNADO_DEMO:
            return R.failed(self, "演示环境，暂无操作权限")
        # 调用更新用户服务方法
        result = await user.UserUpdate(self)
        # 返回结果
        return result


# 删除用户
class UserDeleteHandler(BaseHandler):
    @permission_required("sys:user:delete")
    async def delete(self, id):
        if TORNADO_DEMO:
            return R.failed(self, "演示环境，暂无操作权限")
        # 调用删除用户服务方法
        result = await user.UserDelete(self, id)
        # 返回结果
        return result


# 设置用户状态
class UserStatusHandler(BaseHandler):
    @permission_required("sys:user:status")
    async def put(self):
        if TORNADO_DEMO:
            return R.failed(self, "演示环境，暂无操作权限")
        # 调用设置用户状态服务方法
        result = await user.UserStatus(self)
        # 返回结果
        return result


# 重置密码
class UserResetPwdHandler(BaseHandler):
    async def put(self):
        if TORNADO_DEMO:
            return R.failed(self, "演示环境，暂无操作权限")
        # 调用设置用户状态服务方法
        result = await user.UserResetPwd(self)
        # 返回结果
        return result
